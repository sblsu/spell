#!/usr/bin/env php
<?php

/**
 * Скрипт для проверки орфографии в модулях проекта.
 */

// Подключаем свой словарь
$pspell_config = pspell_config_create('rusls', 'russian', '', 'utf-8');
pspell_config_ignore($pspell_config, 4);
$pspell = pspell_new_config($pspell_config);
if ($pspell === false) {
    echo  "\nCant create pspell link\n";
    exit(1);
}

// Массив орфографических ошибок
$badWords = [];

function spellCheck(string $filePath, $pspellLink, &$badWords) {
    $content = file_get_contents($filePath);
    if ($content === false) {
        return;
    }

    $found = [];
    preg_match_all('/([а-яёА-ЯЁ]+-?[а-яёА-ЯЁ]+)/um', $content, $found);
    foreach ($found as $wordArray) {
        foreach ($wordArray as $word) {
            if (!isset($badWords[ $word ])) {
                if (!pspell_check($pspellLink, $word)) {
                    $rightWords = array_slice(
                        pspell_suggest($pspellLink, $word), 0, 4
                    );

                    $badWords[ $word ]['file'][] = $filePath;
                    $fixWord = '';
                    $variants = '';
                    if (count($rightWords) > 0) {
                        $fixWord = $rightWords[0];
                        $variants = implode(',', $rightWords);
                    }
                    $badWords[ $word ][ 'fixWord'] = $fixWord;
                    $badWords[ $word ][ 'right' ] = $variants;
                    $badWords[ $word ][ 'fix'][] = "sed -e 's/$word/$fixWord/g' $filePath > /tmp/spellFix && mv /tmp/spellFix $filePath";
                }
            } else {
                if (!in_array($filePath, $badWords[ $word ]['file'])) {
                    $badWords[ $word ]['file'][] = $filePath;
                    $fixWord = $badWords[ $word ]['fixWord'];
                    $badWords[ $word ]['fix'][] = "sed -e 's/$word/$fixWord/g' $filePath > /tmp/spellFix && mv /tmp/spellFix $filePath";
                }
            }
        }
    }
}

function scanPath(string $path, $pspellLink, &$badWords) {
    $fiels = scandir($path);
    if ($fiels === false) {
        return;
    }

    foreach ($fiels as $file) {
        if ($file == '.' || $file == '..') {
            continue;
        }

        $file = $path.'/'.$file;
        if (is_dir($file)) {
            scanPath($file, $pspellLink, $badWords);
        }

        if (is_file($file) && preg_match('/\.(?:php|html)$/', $file)) {
            spellCheck($file, $pspellLink, $badWords);
        }
    }
}

function printReport($badWords) {
    foreach (array_keys($badWords) as $word) {
        $h = $badWords[ $word ]['right'] ?? '';
        foreach ($badWords[ $word ]['fix'] as $f) {
            echo  "$f\n";
            if(!empty($h)) {
                echo  "#$h\n";
            }
            echo  "\n";
        }
    }
}

// Ищем опечатки
scanPath('/home/dev/sls/frontend', $pspell, $badWords);
scanPath('/home/dev/sls/backend', $pspell, $badWords);
scanPath('/home/dev/sls/common', $pspell, $badWords);
scanPath('/home/dev/sls/components', $pspell, $badWords);
scanPath('/home/dev/sls/console', $pspell, $badWords);
scanPath('/home/dev/sls/lib', $pspell, $badWords);

// Печатаем отчет
printReport($badWords);
